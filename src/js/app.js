import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const ul = document.querySelector("ul");
  

  const getPokemon = async () => {
    const url = `https://pokeapi.co/api/v2/pokemon?limit=10`;
    const response = await fetch(url);
    const pokemon = await response.json();
    console.log(pokemon.results);

    pokemon.results.map(poke => {
      let liEl = document.createElement('li');
      liEl.innerText = `${poke.name}`
      ul.appendChild(liEl);
    })

  }

  getPokemon();
});


  // createPokemonList = data => {
  //   const pokedexListLength = data.length;
  // }

  // for (let i = 0; i < pokedexListLength; i++) {
  //   const pokemonItem = document.createElement('li');
  //   const pokemonName = data[i].name;
  //   const pokemonUrl = data[i].url;

  //   pokemonItem.appendChild(pokemonName);
  //   ul.appendChild(pokemonItem);

  //}



